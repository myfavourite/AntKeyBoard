//
//  AntKeyBoard.m
//  antQueen
//
//  Created by yixiuge on 16/9/23.
//  Copyright © 2016年 yibyi. All rights reserved.
//

#import "AntKeyBoard.h"
#import "UIView+HDAddition.h"
//#import "NSString+SCAddition.h"
@interface AntKeyBoard () {
//    RGBA(246,81,88,1)
    UIButton    *tipsBtn;
    UIButton    *pastBtn;
    UIButton    *copyBtn;
    NSTimer     *countTimer;
}

@property (nonatomic, strong)NSArray  *uppersArr;

@property (nonatomic, weak)UITextField  *sourceTf;
@end

@implementation AntKeyBoard


- (id)initWithTf:(UITextField *_Nonnull)textField {
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, SCREEN_W, SCREEN_W * 235.0 / SCREEN_W);
        self.sourceTf = textField;
        self.backgroundColor = RGBA(244,244,241,1);
        [self creatUI];
    }
    return self;
}

- (void)creatUI {
    
    CGFloat  space = 6.0;
    CGFloat   ITEM_W = (self.HDview_w - space * 11) / 10.0;
    CGFloat   ITEM_H = (self.HDview_h - space * 6) / 5.0;
    UIFont  *titleFont = [UIFont systemFontOfSize:15.0];
    for (int a = 0; a < 20; a++) {
        UIButton  *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"ant_item_back"] forState:UIControlStateNormal];
        button.frame = CGRectMake(space + (space + ITEM_W) * (a % 10), space + (space + ITEM_H) * (a / 10), ITEM_W, ITEM_H);
        button.titleLabel.font = titleFont;
        [button setTitle:self.uppersArr[a] forState:UIControlStateNormal];
        [self addSubview:button];
        [button addTarget:self action:@selector(characterTouchAction:) forControlEvents:UIControlEventTouchDown];
    }
    
    CGFloat space_x = (self.HDview_w - ITEM_W * 9 - space * 8) / 2.0;
    for (int b = 20; b < 29; b++) {
        UIButton  *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"ant_item_back"] forState:UIControlStateNormal];
        button.frame = CGRectMake(space_x + (space + ITEM_W) * (b - 20), space + (space + ITEM_H) * 2, ITEM_W, ITEM_H);
        button.titleLabel.font = titleFont;
        [button setTitle:self.uppersArr[b] forState:UIControlStateNormal];
        [self addSubview:button];
        [button addTarget:self action:@selector(characterTouchAction:) forControlEvents:UIControlEventTouchDown];
    }
    
//    CGFloat space_x1 = (self.HDview_w - ITEM_W * 9 - space * 8) / 2.0;
    for (int b = 29; b < self.uppersArr.count; b++) {
        UIButton  *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"ant_item_back"] forState:UIControlStateNormal];
        button.frame = CGRectMake(space_x + (space + ITEM_W) * (b - 28), space + (space + ITEM_H) * 3, ITEM_W, ITEM_H);
        button.titleLabel.font = titleFont;
        [button setTitle:self.uppersArr[b] forState:UIControlStateNormal];
        [self addSubview:button];
        [button addTarget:self action:@selector(characterTouchAction:) forControlEvents:UIControlEventTouchDown];
    }
    
    UIImageView *upImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ant_item_up"]];
    upImageView.frame = CGRectMake(space, space + (space + ITEM_H) * 3, space_x + ITEM_W - space, ITEM_H);
    [self addSubview:upImageView];
    
    UIButton   *deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.frame = CGRectMake(space_x + (space + ITEM_W) * 8, space + (space + ITEM_H) * 3, self.HDview_w - (space_x + (space + ITEM_W) * 8) - 8 , ITEM_H);
    [deleteBtn setBackgroundImage:[UIImage imageNamed:@"ant_item_delete"] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:deleteBtn];
    [deleteBtn addTarget:self action:@selector(buttonLongPress:) forControlEvents:UIControlEventTouchDown];
    [deleteBtn addTarget:self action:@selector(countNumTimerCancel) forControlEvents:UIControlEventTouchUpOutside];
    [deleteBtn addTarget:self action:@selector(countNumTimerCancel) forControlEvents:UIControlEventTouchCancel];

    
    copyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [copyBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    copyBtn.frame = CGRectMake(space, space + (space + ITEM_H) * 4, upImageView.HDview_w, ITEM_H);
    copyBtn.titleLabel.font = titleFont;
    copyBtn.clipsToBounds = YES;
    copyBtn.backgroundColor = RGBA(249, 249, 249, 1);
    copyBtn.layer.cornerRadius = 8.0;
    [copyBtn setTitle:@"复制" forState:UIControlStateNormal];
    [self addSubview:copyBtn];
    if ([self isNotEmpty:self.sourceTf.text]) {
        copyBtn.enabled = YES;
        copyBtn.backgroundColor = RGBA(246,81,88,1);
        [copyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    }else {
        
        copyBtn.enabled = NO;

    }
    [copyBtn addTarget:self action:@selector(copyClick:) forControlEvents:UIControlEventTouchUpInside];
    
    pastBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [pastBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    pastBtn.frame = CGRectMake(copyBtn.HDview_x_r + space, space + (space + ITEM_H) * 4, upImageView.HDview_w, ITEM_H);
    pastBtn.titleLabel.font = titleFont;
    pastBtn.clipsToBounds = YES;
    pastBtn.backgroundColor = RGBA(246,81,88,1);
    pastBtn.layer.cornerRadius = 8.0;
    [pastBtn setTitle:@"粘贴" forState:UIControlStateNormal];
    [self addSubview:pastBtn];
    [pastBtn addTarget:self action:@selector(pasteClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [pastBtn addTarget:self action:@selector(btnDown:) forControlEvents:UIControlEventTouchDown];
    [pastBtn addTarget:self action:@selector(btnOutside:) forControlEvents:UIControlEventTouchUpOutside];
    [pastBtn addTarget:self action:@selector(btnCancel:) forControlEvents:UIControlEventTouchCancel];
    
    [copyBtn addTarget:self action:@selector(btnDown:) forControlEvents:UIControlEventTouchDown];
    [copyBtn addTarget:self action:@selector(btnOutside:) forControlEvents:UIControlEventTouchUpOutside];
    [copyBtn addTarget:self action:@selector(btnCancel:) forControlEvents:UIControlEventTouchCancel];
    
    UIButton  *findBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [findBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    findBtn.frame = CGRectMake(self.HDview_w - (upImageView.HDview_w + ITEM_W + space) - space, space + (space + ITEM_H) * 4, upImageView.HDview_w + ITEM_W + space, ITEM_H);
    findBtn.titleLabel.font = titleFont;
    findBtn.clipsToBounds = YES;
    findBtn.backgroundColor = RGBA(246,81,88,1);
    findBtn.layer.cornerRadius = 8.0;
    
    [findBtn setTitle:@"查询" forState:UIControlStateNormal];
    [findBtn addTarget:self action:@selector(findClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:findBtn];
    
    UIButton     *tipsLabel = [[UIButton alloc]initWithFrame:CGRectMake(pastBtn.HDview_x_r + space, pastBtn.HDview_y_t, findBtn.HDview_x_l - pastBtn.HDview_x_r - space * 2, ITEM_H)];
    tipsLabel.clipsToBounds = YES;
    tipsLabel.layer.cornerRadius = 8.0;
    tipsLabel.backgroundColor = RGBA(249, 249, 249, 1);
    [tipsLabel setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    tipsLabel.titleLabel.font = [UIFont systemFontOfSize:14.0];

    [self addSubview:tipsLabel];
    
    NSString  *title = @"请输入vin码";
    if(self.sourceTf.text.length > 0){
        title = [NSString  stringWithFormat:@"已输入%ld位,还差%lu位",(long)self.sourceTf.text.length,(17 - self.sourceTf.text.length)];
    }
    tipsBtn = tipsLabel;
    [tipsLabel setTitle:title forState:UIControlStateNormal];
    [self.sourceTf addTarget:self action:@selector(textFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
}

- (void)btnDown:(UIButton *)sender {
    [sender setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    sender.backgroundColor = RGBA(249, 249, 249, 1);
}

- (void)btnCancel:(UIButton *)sender {
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sender.backgroundColor = RGBA(246,81,88,1);
}

- (void)btnOutside:(UIButton *)sender {
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sender.backgroundColor = RGBA(246,81,88,1);
}

-(void)buttonLongPress:(UIButton*)button
{
    [self countTimerBegin];
}


-(void)countTimerBegin
{
//    [[self class]cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(commitTimer) withObject:nil afterDelay:0.6];
}

-(void)countNumTimerCancel
{
    [[self class]cancelPreviousPerformRequestsWithTarget:self];
    if (countTimer) {
        [countTimer invalidate];
        
        countTimer = nil;
    }
}

-(void)commitTimer
{
    if (!countTimer) {
        NSRunLoop *loop = [NSRunLoop currentRunLoop];
        countTimer = [NSTimer timerWithTimeInterval:0.1 target:self selector:@selector(countNum) userInfo:nil repeats:YES];
        [loop addTimer:countTimer forMode:NSDefaultRunLoopMode];
    }
    
}

-(void)countNum {
    if (self.sourceTf) {
        [self.sourceTf deleteBackward];
    }
}



- (void)findClick {
    
    if (self.returnClick) {
        self.returnClick();
    }
}

- (void)copyClick:(UIButton *)sender {
    
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sender.backgroundColor = RGBA(246,81,88,1);
    
    if (self.sourceTf.text.length > 0) {
        UIPasteboard *pastboard = [UIPasteboard generalPasteboard];
        pastboard.string = self.sourceTf.text;
    }
}

- (void)pasteClick:(UIButton *)sender {
    
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sender.backgroundColor = RGBA(246,81,88,1);
    UIPasteboard *pastboard = [UIPasteboard generalPasteboard];
    NSString  *str = [pastboard.string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (str.length > 0) {
        
        if (_sourceTf.delegate && [_sourceTf.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
            
            for (int a = 0; a < str.length ; a++) {
                NSString *subStr = [str substringWithRange:NSMakeRange(a, 1)];
                NSRange range = NSMakeRange(_sourceTf.text.length, 1);
                BOOL ret = [_sourceTf.delegate textField:_sourceTf shouldChangeCharactersInRange:range replacementString:subStr];
                if (ret) {
                    [_sourceTf insertText:subStr];
                }
            }
            

        }else {
            [self.sourceTf insertText:str];
        }
        

    }
}

- (void)textFieldTextChanged:(UITextField*)textField {
//    NSLog(@"___%@",textField.text);
    NSString  *title = @"请输入vin码";
    
    if(self.sourceTf.text.length > 0){
        title = [NSString  stringWithFormat:@"已输入%lu位,还差%lu位",self.sourceTf.text.length,(17 - self.sourceTf.text.length)];
        copyBtn.enabled = YES;
        [copyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        copyBtn.backgroundColor = RGBA(246,81,88,1);
    } else {
        copyBtn.enabled = NO;
        [copyBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        copyBtn.backgroundColor = RGBA(249, 249, 249, 1);
    }
    
    [tipsBtn setTitle:title forState:UIControlStateNormal];

}

- (void)characterTouchAction:(UIButton *)sender {
    NSString *title = [sender titleLabel].text;
    if (self.sourceTf) {
        UITextField *tmp = (UITextField *)self.sourceTf;
        
        if (tmp.delegate && [tmp.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
            NSRange range = NSMakeRange(tmp.text.length, 1);
            BOOL ret = [tmp.delegate textField:tmp shouldChangeCharactersInRange:range replacementString:title];
            if (ret) {
                [tmp insertText:title];
            }
        }else{
            [tmp insertText:title];
        }
    }
}

- (void)deleteBtnAction:(UIButton *)sender {
    [self countNumTimerCancel];
    if (self.sourceTf) {
        
        [self.sourceTf deleteBackward];
    }
}

- (NSArray *)uppersArr {
    if (!_uppersArr) {
        _uppersArr = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"0",@"Q",@"W",@"E",@"R",@"T",@"Y",@"U",@"I",@"O",@"P",@"A",@"S",@"D",@"F",@"G",@"H",@"J",@"K",@"L",@"Z",@"X",@"C",@"V",@"B",@"N",@"M"];
    }
    return _uppersArr;
}

- (BOOL)isNotEmpty:(NSString *)str;
{
    return (![(NSNull *)str isEqual:[NSNull null]]
            && [str isKindOfClass:[NSString class]]
            && str.length > 0);
}

@end
