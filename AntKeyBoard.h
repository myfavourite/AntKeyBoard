//
//  AntKeyBoard.h
//  antQueen
//
//  Created by yixiuge on 16/9/23.
//  Copyright © 2016年 yibyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

typedef void(^ReturnClick)();

@interface AntKeyBoard : UIView

@property (nonatomic, copy)ReturnClick  returnClick;

- (id)initWithTf:(UITextField *)textField;

@end
